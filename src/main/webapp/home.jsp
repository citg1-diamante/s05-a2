<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Home</title>
	</head>
	<body>
		<%
			String message = "";
			String type = session.getAttribute("regType").toString();
			String name = session.getAttribute("fullname").toString();
			if(type.equals("Applicant")){
				message = "Welcome Applicant! You may now start looking for career opportunity ";
			}
			else{
				message = "Welcome Employer! You may now start browsing applicant profiles ";
			}
		%>
		
		<h1>Welcome <%= name %>!</h1>
		<p><%= message %></p>
	</body>
</html>
