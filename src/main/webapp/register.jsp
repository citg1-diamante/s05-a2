<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Confirmation</title>
	</head>
	<body>
		<h1>Registration Confirmation</h1>
		
			<p>First Name: <%= session.getAttribute("fname") %></p>
			<p>Last Name: <%= session.getAttribute("lname") %></p>
			<p>Phone Number: <%= session.getAttribute("phone") %></p>
			<p>Email: <%= session.getAttribute("email") %></p>
			<p>App Discovery: <%= session.getAttribute("discovery") %></p>
			<p>Date of Birth: <%= session.getAttribute("dob") %></p>
			<p>User Type: <%= session.getAttribute("regType") %></p>
			<p>Description: <%= session.getAttribute("profile") %></p>
		<form action="login" method="post">
			<input type="submit">
		</form>
	
	<!-- Back button to index.jsp -->
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
	</body>
</html>
